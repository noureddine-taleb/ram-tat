module.exports = {
  mode: "jit",
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  textColor: {
    red: "#c20831",
    green: "#2ad4b9",
    yellow: "#fdbc64",
    blue: "#648cb2",
  },
  theme: {
    extend: {
      colors: {
        primary: "#c7292e",
        secondary: "#d5043321",
        "ram-red": "#c3022d",
        "ram-green": "#4ed19e",
        "ram-shiraz": "#c20831",
      },
      fontFamily: {
        lato: "Lato",
      },
    },
  },
};
