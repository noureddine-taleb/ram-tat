import { DashboardLayout } from "layouts/DashboardLayout";
import { Navigate, useRoutes } from "react-router-dom";
import { NotFound } from "views/404";
import { Bagage } from "views/FlightDetail/components/Bagage";
import { Charge } from "views/FlightDetail/components/Charge";
import { FlightDetailIndex } from "views/FlightDetail/components/FlightDetailIndex";
import { Fuel } from "views/FlightDetail/components/Fuel";
import { Passengers } from "views/FlightDetail/components/Passengers";
import { FlightDetail } from "views/FlightDetail/FlightDetail";
import { TaskList } from "views/TaskList/TaskList";
import { MyTasks } from "views/TatDetails/components/MyTasks";
import { Resources } from "views/TatDetails/Resources";
import { Slot } from "views/TatDetails/Slot";
import { TatDetails } from "views/TatDetails/TatDetails";
import { TeamVue } from "views/TatDetails/TeamVue";
import { TatList } from "views/TatList/TatList";

const flightDetailViews = [
  { path: "", element: <FlightDetailIndex /> },
  { path: "passengers", element: <Passengers /> },
  { path: "bagage", element: <Bagage /> },
  { path: "charge", element: <Charge /> },
  { path: "fuel", element: <Fuel /> },
];

const tatDetailsViews = [
  { path: "", element: <MyTasks /> },
  { path: "vue-equipe", element: <TeamVue /> },
  { path: "resources", element: <Resources /> },
  {
    path: "vol-infos",
    element: <FlightDetail />,
    children: flightDetailViews,
  },
  { path: "slot", element: <Slot /> },
];

export function Routes() {
  return useRoutes([
    { path: "/", element: <Navigate to="/dashboard/tat" /> },
    { path: "/dashboard", element: <Navigate to="/dashboard/tat" /> },
    {
      path: "404",
      element: <NotFound />,
    },
    {
      path: "/dashboard/tat",
      element: <DashboardLayout />,
      children: [
        { path: "", element: <TatList /> },
        {
          path: ":tatId",
          element: <TatDetails />,
          children: tatDetailsViews,
        },
      ],
    },
    {
      path: "/dashboard/task",
      element: <DashboardLayout />,
      children: [{ path: "", element: <TaskList /> }],
    },

    { path: "*", element: <Navigate to="/404" replace /> },
  ]);
}
