// https://create-react-app.dev/docs/proxying-api-requests-in-development#configuring-the-proxy-manually
// https://github.com/chimurai/http-proxy-middleware
const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    [
      "/tatBusiness",
      "/api",
      "/logout",
      "/login",
      "/private",
      "/oauth2/authorization/tat-realm",
      "/login/oauth2/code/tat-realm",
      "/token",
    ],
    createProxyMiddleware({
      target: "http://192.168.1.102:8090",
      changeOrigin: true,
      xfwd: true,
    })
  );
};
