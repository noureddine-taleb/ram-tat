import React from "react";

export function NotFound() {
  return (
    <div className="flex w-[100vw] h-[100vh] justify-center items-center">
      <div className="font-extrabold text-primary">Not Found</div>
    </div>
  );
}
