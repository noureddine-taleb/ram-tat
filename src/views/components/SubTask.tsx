import React from "react";
import {
  getProgressBgColor,
  getProgressTextColor,
  getProgressDesc,
} from "helpers/tatHelpers";
import { TatProgressBar } from "./TatProgressBar";
//import {LabelImportant} from '@material-ui/icons';

export function SubTask({
  title,
  className,
  progress,
  startTime,
  endTime,
}: {
  title: string;
  className?: string;
  progress: number;
  startTime: string;
  endTime: string;
}) {
  return (
    <div className={className}>
      <div className="flex gap-3 w-full items-center">
        <div>{/*<LabelImportant />*/}</div>
        <div className="flex">
          <div className="font-bold">{title}</div>
        </div>
      </div>

      <TatProgressBar
        progress={progress}
        startTime={startTime}
        endTime={endTime}
      ></TatProgressBar>

      <div className="flex gap-3 w-full font-semibold justify-between lg:justify-around items-center">
        <div className={`${getProgressTextColor(progress)}`}>-- --</div>
        <button
          className={`${getProgressBgColor(
            progress
          )} min-w-24 h-9 p-2 rounded-lg font-semibold text-white flex justify-center items-center`}
        >
          {getProgressDesc(progress)}
        </button>
      </div>
    </div>
  );
}
