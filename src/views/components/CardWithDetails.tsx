import React, { useState } from "react";

export function CardWithDetails({
  main,
  details,
}: {
  main: React.ReactNode;
  details: React.ReactNode;
}) {
  const [show, setShow] = useState(false);

  return (
    <div className="max-w-full font-lato w-full">
      <div className="flex flex-col items-center m-5">
        <div
          style={{ zIndex: 1 }}
          className="flex lg:flex-row flex-col items-center justify-between gap-y-8 bg-[#f4f4f4] rounded-md w-full shadow-lg p-8 mt-3 pt-8"
        >
          {main}
          <div className="flex items-end justify-center">
            <button
              onClick={() => setShow(!show)}
              className="min-w-[40px] h-10 rounded-full bg-gradient-to-tr from-[#e44e56] to-[#efafb5] shadow-2xl flex justify-center items-center"
            >
              <img
                className={`w-5 transition-all duration-500 ${
                  show && "rotate-180"
                }`}
                src="/static/icons8-chevron-left-64px-copie.png"
                alt=""
              />
            </button>
          </div>
        </div>
        {show && (
          <div
            className={`flex flex-col w-[99%] rounded-b-md shadow-lg p-4 bg-[#f4f4f4] transition-all duration-200 h-auto min-h-0`}
          >
            {details}
          </div>
        )}
      </div>
    </div>
  );
}
