import React, {useState} from 'react';
import {IconNext} from 'components/Icons/IconNext';
import {IconPrev} from 'components/Icons/IconPrev';
import {Paging} from 'shared/models/Response';
import {Filter} from 'shared/models/Filter';
import {NB_PAGE_AT_A_TIME} from 'config/config';

export function Pagination({
	paging,
	filters,
	setFilters,
}: {
	paging: Paging;
	filters: Filter;
	setFilters: React.Dispatch<React.SetStateAction<Filter>>;
}) {
	const [start, setStart] = useState(0);

	const clickHandler = (i: number) => {
		return () => setFilters(filters.setPageNumber(i));
	};

	return (
		<div>
			{!!paging.totalPages && (
				<div className="flex gap-1 mt-2">
					{start > 0 && (
						<button
							onClick={() => setStart(Math.max(0, start - NB_PAGE_AT_A_TIME))}
							className="text-[#C20831] border border-[#C20831] rounded-lg w-[fit-content] h-9 flex items-center justify-center px-1">
							<IconPrev className="w-4 h-4 text-[#C20831]" />
							Précédent
						</button>
					)}
					{(() => {
						let arr = [];
						for (
							let i = start;
							i < Math.min(paging.totalPages, start + NB_PAGE_AT_A_TIME);
							i++
						) {
							arr.push(
								i === paging.index ? (
									<button
										onClick={clickHandler(i)}
										key={i}
										className="text-white border border-[#C20831] bg-[#C20831] rounded-lg w-9 h-9 flex items-center justify-center">
										{i + 1}
									</button>
								) : (
									<button
										onClick={clickHandler(i)}
										key={i}
										className="text-[#C20831] border-[#C20831] border bg-white rounded-lg w-9 h-9 flex items-center justify-center">
										{i + 1}
									</button>
								)
							);
						}
						return arr;
					})()}
					{Math.min(paging.totalPages, start + NB_PAGE_AT_A_TIME) <
						paging.totalPages && (
						<button
							onClick={() =>
								setStart(
									Math.min(paging.totalPages - 1, start + NB_PAGE_AT_A_TIME)
								)
							}
							className="text-[#C20831] border border-[#C20831] rounded-lg w-[fit-content] h-9 flex items-center justify-center px-1">
							Prochain
							<IconNext className="w-4 h-4 text-[#C20831]" />
						</button>
					)}
				</div>
			)}
		</div>
	);
}
