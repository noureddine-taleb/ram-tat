import React from "react";
import { getProgressBgColor, getProgressTextColor } from "helpers/tatHelpers";

export function TatProgressBar({
  className,
  progress,
  startTime,
  endTime,
}: {
  className?: string;
  progress: number;
  startTime: string;
  endTime: string;
}) {
  return (
    <div className={`flex flex-col justify-end w-full h-[97px] ${className}`}>
      <div className="bg-gray-400 rounded-lg h-3 w-full">
        <div
          className={`${getProgressBgColor(progress)} h-full rounded-lg`}
          style={{ width: `${progress}%` }}
        ></div>
      </div>
      <div className="flex justify-between">
        <div className={`mt-2 ${getProgressTextColor(progress)}`}>
          {startTime}
        </div>
        <div className={`mt-2 ${getProgressTextColor(progress)}`}>
          {endTime}
        </div>
      </div>
    </div>
  );
}
