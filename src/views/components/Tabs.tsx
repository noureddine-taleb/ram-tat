import { TabElement } from "interfaces/TabElement";
import React from "react";
import { Link } from "react-router-dom";

interface TabsProps {
  className?: string;
  value: string;
  items: TabElement[];
}

export function Tabs({ className, value, items }: TabsProps) {
  const isSelected = (v: string) => {
    return v === value;
  };

  return (
    <div
      className={`bg-gradient-to-r from-[#f0405c] to-[#c20831] w-full font-bold flex justify-center gap-6 text-white fixed top-36 z-20 ${className}`}
    >
      {items.map((i) => (
        <Link to={i.to} key={i.to}>
          <div
            className={`flex flex-col gap-y-2 ${
              isSelected(i.to) && "text-black border-b-2 border-black"
            }`}
          >
            <div className="flex justify-center">{i.icon}</div>
            <div>{i.label}</div>
          </div>
        </Link>
      ))}
    </div>
  );
}
