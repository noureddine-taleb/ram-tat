import React from "react";
import { Link, Outlet, useLocation } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function FlightDetail() {
  const location = useLocation();
  const getLinkColor = (path: string) =>
    path === (location.pathname.split("/")[5] || "")
      ? "text-primary"
      : "text-black";

  return (
    <div className="flex flex-col">
      <Outlet />
      <div className="fixed bottom-0 w-[81%] z-10 flex justify-around bg-white rounded-md px-4 py-2 overflow-hidden font-semibold shadow-xl mb-8 h-16 items-center">
        <Link to="">
          <div
            className={`uppercase flex flex-col justify-center items-center cursor-pointer ${getLinkColor(
              ""
            )}`}
          >
            <FontAwesomeIcon icon="plane-departure"></FontAwesomeIcon>
            <div>infos vol</div>
          </div>
        </Link>
        <Link to="passengers">
          <div
            className={`uppercase flex flex-col justify-center items-center cursor-pointer ${getLinkColor(
              "passengers"
            )}`}
          >
            <FontAwesomeIcon icon="user"></FontAwesomeIcon>
            <div>passengers</div>
          </div>
        </Link>
        <Link to="bagage">
          <div
            className={`uppercase flex flex-col justify-center items-center cursor-pointer ${getLinkColor(
              "bagage"
            )}`}
          >
            <FontAwesomeIcon icon="suitcase"></FontAwesomeIcon>
            <div>bagages</div>
          </div>
        </Link>
        <Link to="charge">
          <div
            className={`uppercase flex flex-col justify-center items-center cursor-pointer ${getLinkColor(
              "charge"
            )}`}
          >
            <FontAwesomeIcon icon="truck-loading"></FontAwesomeIcon>
            <div>charge</div>
          </div>
        </Link>
        <Link to="fuel">
          <div
            className={`uppercase flex flex-col justify-center items-center cursor-pointer ${getLinkColor(
              "fuel"
            )}`}
          >
            <FontAwesomeIcon icon="gas-pump"></FontAwesomeIcon>
            <div>fuel</div>
          </div>
        </Link>
      </div>
    </div>
  );
}
