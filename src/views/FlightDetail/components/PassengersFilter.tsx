import React, { useState } from "react";
import { Button } from "views/TatDetails/components/Button";

export function PassengersFilter() {
  const [clicked, setClicked] = useState(false);
  const [clicked2, setClicked2] = useState(false);

  return (
    <div className="bg-gray-300 p-2 rounded-md flex flex-col gap-2">
      <div className="bg-gray-200 h-8 flex justify-center items-center rounded-md">
        <Button
          fullWidth
          onClick={() => setClicked(!clicked)}
          variant={clicked ? "contained" : "text"}
        >
          Map
        </Button>
        <Button
          fullWidth
          onClick={() => setClicked2(!clicked2)}
          variant={clicked2 ? "contained" : "text"}
        >
          List
        </Button>
      </div>
      <div className="flex h-10">
        <input
          className="w-full rounded-md pl-4"
          placeholder="Chercher par FN ou AC_REGISTRATION"
        />
      </div>
      <div className="flex flex-col justify-center items-center bg-secondary p-2 gap-2 rounded-md">
        <div className="text-primary font-bold">Filter</div>
        <div className="h-[2px] bg-primary w-full"></div>
        <div className=" flex flex-col gap-2 w-full">
          <Button
            fullWidth
            className="border-white"
            onClick={() => setClicked(!clicked)}
            variant={clicked ? "contained" : "outlined"}
          >
            All
          </Button>
          <Button
            fullWidth
            className="border-white"
            onClick={() => setClicked(!clicked)}
            variant={clicked ? "contained" : "outlined"}
          >
            Loyalty
          </Button>
          <Button
            fullWidth
            className="border-white"
            onClick={() => setClicked(!clicked)}
            variant={clicked ? "contained" : "outlined"}
          >
            VIP
          </Button>
          <Button
            fullWidth
            className="border-white"
            onClick={() => setClicked(!clicked)}
            variant={clicked ? "contained" : "outlined"}
          >
            lorem
          </Button>
          <Button
            fullWidth
            className="border-white"
            onClick={() => setClicked(!clicked)}
            variant={clicked ? "contained" : "outlined"}
          >
            lorem
          </Button>
          <Button
            fullWidth
            className="border-white"
            onClick={() => setClicked(!clicked)}
            variant={clicked ? "contained" : "outlined"}
          >
            lorem
          </Button>
          <Button
            fullWidth
            className="border-white"
            onClick={() => setClicked(!clicked)}
            variant={clicked ? "contained" : "outlined"}
          >
            lorem
          </Button>
        </div>
      </div>
    </div>
  );
}
