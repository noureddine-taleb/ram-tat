import React from 'react';
import {PassengerMap} from './PassengerMap/PassengerMap';
import {PassengersFilter} from './PassengersFilter';
export function Passengers() {
	return (
		<div className="flex gap-4">
			<div className="w-1/3">
				<PassengersFilter />
			</div>
			<div className="flex justify-center mx-auto relative -top-28">
				<PassengerMap />
			</div>
		</div>
	);
}
