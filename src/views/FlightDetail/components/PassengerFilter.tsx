import React, { useState } from "react";
import { Button } from "views/TatDetails/components/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function PassengerFilter() {
  const [clicked, setClicked] = useState(false);
  const [clicked2, setClicked2] = useState(false);
  const [show, setShow] = useState(false);

  return (
    <div className="flex flex-col">
      <div className="flex flex-col gap-3 bg-gray-100 rounded-md overflow-hidden">
        <div className="bg-gray-200 h-10 flex justify-center items-center rounded-md">
          <div className="flex gap-1 h-8 w-[99%]">
            <Button fullWidth variant={"text"}>
              Vol Arrivee
            </Button>
            <Button fullWidth variant={"text"}>
              Vol Depart
            </Button>

            <Button
              className="w-20"
              onClick={() => setShow(!show)}
              variant={clicked2 ? "contained" : "outlined"}
            >
              <FontAwesomeIcon icon="filter" className="w-6 h-6" />
            </Button>
          </div>
        </div>
        <div
          className={`flex flex-col justify-center items-center pb-5 gap-4 ${
            !show && "hidden"
          }`}
        >
          <div className="flex w-[99%] gap-4">
            <div className="flex flex-col w-full">
              <div className="font-bold pb-2">NOM</div>
              <div className="flex gap-2">
                <input className="w-full h-8 rounded-md"></input>
              </div>
            </div>
            <div className="flex flex-col w-full">
              <div className="font-bold pb-2">CLASSE</div>
              <div className="flex gap-2">
                <select className="w-full h-8 rounded-md">
                  <option value="test">test</option>
                  <option value="test">test</option>
                  <option value="test">test</option>
                </select>
              </div>
            </div>
            <div className="flex flex-col w-full">
              <div className="font-bold pb-2">SIEGE</div>
              <div className="flex gap-2">
                <Button className="w-full h-8" variant="outlined">
                  Taches En Retard
                </Button>
              </div>
            </div>
            <div className="flex flex-col w-full">
              <div className="font-bold pb-2">SPECIAL REQ</div>
              <div className="flex gap-2">
                <select className="w-full h-8 rounded-md">
                  <option value="test">test</option>
                </select>
              </div>
            </div>
          </div>
          <div className="flex gap-1 w-[99%]">
            <div className="flex flex-col w-full">
              <div className="font-bold pb-2">STATUT</div>
              <div className="flex gap-2 w-full">
                <Button
                  className="h-8"
                  fullWidth
                  onClick={() => setClicked(!clicked)}
                  variant={clicked ? "contained" : "outlined"}
                >
                  BOARDED
                </Button>
                <Button
                  className="h-8"
                  fullWidth
                  onClick={() => setClicked2(!clicked2)}
                  variant={clicked2 ? "contained" : "outlined"}
                >
                  NOT BOARDED
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
