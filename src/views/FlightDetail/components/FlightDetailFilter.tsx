import React from "react";
import { TaskFilter } from "shared/models/TaskFilter";
import { Button } from "views/TatDetails/components/Button";

interface FilterProps {
  filter: TaskFilter;
  setFilter: (f: TaskFilter) => void;
}

export function FlightDetailFilter({ filter, setFilter }: FilterProps) {
  return (
    <div className="flex flex-col">
      <div className="flex flex-col gap-3 bg-gray-100 rounded-md overflow-hidden">
        <div className="bg-gray-200 h-10 flex justify-center items-center rounded-md">
          <div className="flex gap-1 h-8 w-[99%]">
            <Button
              fullWidth
              onClick={() => setFilter(filter.setPhase("ARRIVEE"))}
              variant={filter.getPhase() === "ARRIVEE" ? "contained" : "text"}
            >
              Vol Arrivee
            </Button>
            <Button
              fullWidth
              onClick={() => setFilter(filter.setPhase("DEPART"))}
              variant={filter.getPhase() === "DEPART" ? "contained" : "text"}
            >
              Vol Depart
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
