import React from "react";
import { busPassengersMap, econPassengersMap } from "data/passengersMap";
import { PassengerDegree } from "./PassengerDegree";
import { PassengerRow } from "./PassengerRow";

export function PassengerMap() {
  return (
    <div className="bg-gray-300 rounded-tl-full rounded-tr-full flex flex-col p-2 w-full justify-between gap-4">
      <PassengerDegree title="economy" bg="bg-primary" className="mt-44" />
      {econPassengersMap.map((row, i) => (
        <PassengerRow key={i} map={row} />
      ))}
      <PassengerDegree title="business" bg="bg-gray-500" />
      {busPassengersMap.map((row, i) => (
        <PassengerRow key={i} map={row} />
      ))}
    </div>
  );
}
