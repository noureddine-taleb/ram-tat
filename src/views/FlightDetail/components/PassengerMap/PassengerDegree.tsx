import React from 'react';

interface Props {
	title: string;
	className?: string;
	bg: string;
}

export function PassengerDegree({title, bg, className}: Props) {
	return (
		<div
			className={`h-5 w-full ${bg} flex items-center text-white rounded-sm overflow-hidden font-bold ${className}`}>
			<div className="h-full w-1 bg-blue-600"></div>
			<div className="pl-4">{title}</div>
		</div>
	);
}
