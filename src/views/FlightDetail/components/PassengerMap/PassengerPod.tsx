import React from "react";
import { SeatStatus } from "interfaces/PassengerSeatMap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

interface PodProps {
  seat: SeatStatus;
}

export function PassengerPod({ seat }: PodProps) {
  return (
    <div className="flex flex-col rounded-lg bg-gray-400 w-12 h-16 overflow-hidden items-center">
      <FontAwesomeIcon
        className={`w-full h-2/3 text-white ${seat.status ? "" : "opacity-0"}`}
        icon="user"
      ></FontAwesomeIcon>
      <div
        className={`${
          seat.status ? "bg-white" : ""
        }  h-1/3 w-full flex justify-center items-center font-bold`}
      >
        {seat.id}
      </div>
    </div>
  );
}
