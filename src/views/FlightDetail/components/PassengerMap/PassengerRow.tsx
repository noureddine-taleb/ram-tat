import React from "react";
import { RowSeatsMap } from "interfaces/PassengerSeatMap";
import { PassengerPod } from "./PassengerPod";

interface RowProp {
  map: RowSeatsMap; // availablity map
}

export function PassengerRow({ map }: RowProp) {
  return (
    <div className="flex justify-between items-center">
      <div className="flex justify-between items-center w-44">
        {map.leftMap.map((p, i) => (
          <PassengerPod key={i} seat={p} />
        ))}
      </div>

      <div className="font-bold text-blue-400 text-2xl mx-5">{map.row}</div>
      <div className="flex justify-between items-center w-44">
        {map.rightMap.map((p, i) => (
          <PassengerPod key={i} seat={p} />
        ))}
      </div>
    </div>
  );
}
