import React from "react";
import { PassengerFilter } from "./PassengerFilter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function Bagage() {
  return (
    <div className="flex flex-col">
      <PassengerFilter />
      <div className="flex flex-col rounded-md shadow-md p-2 mt-4 bg-gray-100">
        <div className="flex bg-[#c7292e] rounded-tr-md rounded-br-md  p-2 w-[fit-content] text-white font-bold justify-center my-2 -left-2 relative">
          Depart AT206-CMN/CMYK/BLU
        </div>
        <table>
          <thead className="bg-white rounded-lg overflow-hidden">
            <tr>
              <th className="px-8"></th>
              <th className="py-4">Nom</th>
              <th className="py-4">Classe</th>
              <th className="py-4">Siege</th>
              <th className="py-4">Statut</th>
              <th className="py-4">Sequence</th>
              <th className="py-4">Special Req</th>
              <th className="py-4">NBR de Bagages</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="py-4 flex justify-center items-center">
                <div className="flex h-full justify-center relative">
                  <div className="bg-black rounded-lg p-2 absolute left-0 -top-2">
                    <FontAwesomeIcon
                      className="text-white"
                      icon="user"
                    ></FontAwesomeIcon>
                  </div>
                </div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
            </tr>
            <tr>
              <td className="py-4 flex justify-center items-center">
                <div className="flex h-full justify-center relative">
                  <div className="bg-black rounded-lg p-2 absolute left-0 -top-2">
                    <FontAwesomeIcon
                      className="text-white"
                      icon="user"
                    ></FontAwesomeIcon>
                  </div>
                </div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
            </tr>
            <tr>
              <td className="py-4 flex justify-center items-center">
                <div className="flex h-full justify-center relative">
                  <div className="bg-black rounded-lg p-2 absolute left-0 -top-2">
                    <FontAwesomeIcon
                      className="text-white"
                      icon="user"
                    ></FontAwesomeIcon>
                  </div>
                </div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}
