import React, { useEffect, useState } from "react";
import { TaskFilter } from "shared/models/TaskFilter";
import { Flight, Tat, TAT_TYPE } from "shared/models/Tat";
import { getTat } from "shared/services/TatService";
import { useParams } from "react-router";
import { FlightDetailFilter } from "./FlightDetailFilter";
import { FlightType } from "shared/models/Response";
import { getFlightTypes } from "shared/services/RefService";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function flightTypeToStr(type?: string | null) {
  switch (type) {
    case "ARRIVEE":
      return "Arrivee";
    case "DEPART":
      return "Depart";
  }
}

export function FlightDetailIndex() {
  const { tatId } = useParams();
  const [filter, setFilter] = useState<TaskFilter>(new TaskFilter("ARRIVEE"));
  const [tat, setTat] = useState<Tat>();
  const [flight, setFlight] = useState<Flight>();
  const [types, setTypes] = useState<FlightType[]>([]);

  useEffect(() => {
    getTat(tatId as string).then(setTat);
    getFlightTypes().then(setTypes);
  }, [tatId]);

  useEffect(() => {
    if (!tat) return;
    if (filter.getPhase() === "ARRIVEE") setFlight(tat.__getArrFlight());
    else setFlight(tat.__getDepFlight());
  }, [filter]);

  useEffect(() => {
    if (!tat) return;

    if (tat.TATType() === TAT_TYPE.TAT_TYPE_ARR) {
      setFilter(filter.setPhase("ARRIVEE"));
    } else if (tat.TATType() === TAT_TYPE.TAT_TYPE_DEP) {
      setFilter(filter.setPhase("DEPART"));
    } else if (tat.TATType() === TAT_TYPE.TAT_TYPE_ARRDEP) {
      setFilter(filter.setPhase("ARRIVEE"));
    }
  }, [tat]);

  return (
    <>
      {tat?.TATType() === TAT_TYPE.TAT_TYPE_ARRDEP && (
        <FlightDetailFilter
          filter={filter}
          setFilter={setFilter}
        ></FlightDetailFilter>
      )}
      <div className="flex flex-col rounded-md shadow-md p-2 mt-4 bg-gray-100 gap-4">
        <div className="flex bg-primary rounded-tr-md rounded-br-md  p-2 w-[fit-content] text-white font-bold justify-center my-2 -left-2 relative">
          {flightTypeToStr(filter.getPhase())} - {flight?.getFN()} -{" "}
          {flight?.["DEP_AP_SCHED"]}/{flight?.["ARR_AP_SCHED"]} -{" "}
          {flight?.getAC()}
        </div>
        <div className="flex justify-between px-4 py-2">
          <div className="flex items-center">
            <img
              className="h-4 mr-2"
              src="/static/icons8-airplane-64px-copie-2.png"
              alt=""
            />
            <div className="text-primary mr-2">Avion</div>
            <div>
              {flight?.["AC_SUBTYPE"]} / {flight?.["AC_VERSION"]}
            </div>
          </div>
          <div className="flex items-center">
            <img
              className="h-4 mr-2"
              src="/static/icons8-parking-and-penthouse-64px-copie-2.png"
              alt=""
            />
            <div className="text-primary mr-2">Parking</div>
            <div></div>
          </div>
          <div className="flex items-center">
            <img
              className="h-4 mr-2"
              src="/static/icons8-tollbooth-64px-copie-2.png"
              alt=""
            />
            <div className="text-primary mr-2">Port</div>
            <div></div>
          </div>
        </div>
        <div className="flex justify-between bg-white rounded-md px-4 py-2 relative overflow-hidden font-semibold">
          <div className="absolute bg-primary h-full w-1 top-0 left-0"></div>
          <div className="text-primary">STD</div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="clock"
            ></FontAwesomeIcon>
            {flight?.getSTD_Time()}
          </div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="calendar"
            ></FontAwesomeIcon>
            {flight?.getSTD_Date()}
          </div>
        </div>
        <div className="flex justify-between bg-white rounded-md px-4 py-2 relative overflow-hidden font-semibold">
          <div className="absolute bg-primary h-full w-1 top-0 left-0"></div>
          <div className="text-primary">STA</div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="clock"
            ></FontAwesomeIcon>
            {flight?.getSTA_Time()}
          </div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="calendar"
            ></FontAwesomeIcon>

            {flight?.getSTA_Date()}
          </div>
        </div>

        <div className="flex justify-between bg-white rounded-md px-4 py-2 relative overflow-hidden font-semibold">
          <div className="absolute bg-primary h-full w-1 top-0 left-0"></div>
          <div className="text-primary">ETD</div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="clock"
            ></FontAwesomeIcon>
            {flight?.getETD_Time()}
          </div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="calendar"
            ></FontAwesomeIcon>

            {flight?.getETD_Date()}
          </div>
        </div>
        <div className="flex justify-between bg-white rounded-md px-4 py-2 relative overflow-hidden font-semibold">
          <div className="absolute bg-primary h-full w-1 top-0 left-0"></div>
          <div className="text-primary">ETA</div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="clock"
            ></FontAwesomeIcon>
            {flight?.getETA_Time()}
          </div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="calendar"
            ></FontAwesomeIcon>

            {flight?.getETA_Date()}
          </div>
        </div>

        <div className="flex justify-between bg-white rounded-md px-4 py-2 relative overflow-hidden font-semibold">
          <div className="absolute bg-primary h-full w-1 top-0 left-0"></div>
          <div className="text-primary">ATD</div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="clock"
            ></FontAwesomeIcon>
            {flight?.getATD_Time() || "-"}
          </div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="calendar"
            ></FontAwesomeIcon>

            {flight?.getATD_Date() || "-"}
          </div>
        </div>
        <div className="flex justify-between bg-white rounded-md px-4 py-2 relative overflow-hidden font-semibold">
          <div className="absolute bg-primary h-full w-1 top-0 left-0"></div>
          <div className="text-primary">ATA</div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="clock"
            ></FontAwesomeIcon>
            {flight?.getATA_Time() || "-"}
          </div>
          <div className="flex justify-center items-center">
            <FontAwesomeIcon
              className="mr-2 text-primary"
              icon="calendar"
            ></FontAwesomeIcon>

            {flight?.getATA_Date() || "-"}
          </div>
        </div>
        <div className="flex bg-white rounded-md px-4 py-2 relative overflow-hidden font-semibold">
          <div className="absolute bg-primary h-full w-1 top-0 left-0"></div>
          <div className="grid grid-cols-3 w-full">
            <div className="text-primary">Type de vol</div>
            <div className="flex font-medium justify-start">
              {types?.find((v) => v.leg === flight?.getLegType())?.code}
            </div>
            <div className="flex font-medium justify-center"></div>
            <div className="text-primary">Statut de vol</div>
            <div className="flex font-medium justify-start">
              {flight?.getLegState()}
            </div>
            <div className="flex font-medium justify-center"></div>

            <div className="text-primary">Code de Ratard 1</div>
            <div className="flex font-medium justify-start">
              {flight?.["DELAY_CODE_01"]}
            </div>
            <div className="flex font-medium justify-center">
              <div className="text-primary font-semibold">Duree</div>
              &nbsp; {parseInt(flight?.["DELAY_TIME_01"] || "")} min
            </div>

            <div className="text-primary">Code de Ratard 2</div>
            <div className="flex font-medium justify-start">
              {flight?.["DELAY_CODE_02"]}
            </div>
            <div className="flex font-medium justify-center">
              <div className="text-primary font-semibold">Duree</div>
              &nbsp; {parseInt(flight?.["DELAY_TIME_02"] || "")} min
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
