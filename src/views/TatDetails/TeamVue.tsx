import { CardWithDetails } from "views/components/CardWithDetails";
import { SubTask } from "views/components/SubTask";
import { TatProgressBar } from "views/components/TatProgressBar";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { TaskFilter } from "shared/models/TaskFilter";
import { TasksFilter } from "./components/TasksFilter";
import { Task } from "shared/models/Task";
import { getTatTasks } from "shared/services/TatService";
import { taskTime } from "shared/helpers/misc";

export function TeamVue() {
  const { tatId } = useParams();
  const [tasks, setTasks] = useState<{ [key: string]: Task[] }>({});
  const [filter, setFilter] = useState<TaskFilter>(
    new TaskFilter(tatId as string)
  );

  useEffect(() => {
    getTatTasks(filter.getQueryParams()).then(setTasks);
  }, [tatId, filter]);

  return (
    <div className="flex flex-col">
      <TasksFilter
        extraFilters
        filter={filter}
        setFilter={setFilter}
        regroup
      ></TasksFilter>
      {Object.keys(tasks).map((group) => (
        <CardWithDetails
          key={group}
          main={
            <>
              <div className="flex justify-between gap-1">
                <span className="font-bold">{group}</span>
              </div>
              <TatProgressBar
                className="w-3/12"
                progress={40}
                startTime={"--:--"}
                endTime={"--:--"}
              ></TatProgressBar>
            </>
          }
          details={
            <>
              {tasks[group].map((task, i) => (
                <>
                  <SubTask
                    key={task.idTask}
                    title={task.name}
                    progress={40}
                    startTime={taskTime(task.startDateCalculated)}
                    endTime={taskTime(task.endDateCalculated)}
                    className="flex lg:flex-row flex-col gap-y-4 w-full items-center bg-[#f4f4f4]"
                  />
                  <div className="w-full h-[2px] bg-[#d7d7d7] my-3"></div>
                </>
              ))}
            </>
          }
        ></CardWithDetails>
      ))}
    </div>
  );
}
