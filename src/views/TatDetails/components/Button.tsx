import React from "react";

interface ButtonProps {
  fullWidth?: boolean;
  variant: "contained" | "outlined" | "text";
  onClick?: () => void;
  className?: string;
  children: any;
}
export function Button({
  fullWidth,
  variant,
  onClick,
  children,
  className,
}: ButtonProps) {
  return (
    <button
      onClick={onClick}
      className={`rounded-md ${fullWidth && "w-full"} ${
        variant === "contained" && "text-white bg-ram-red"
      } ${
        variant === "outlined" && "text-ram-red border-[1px] border-ram-red"
      } ${variant === "text" && "text-ram-red"} h-8 ${className}`}
    >
      {children}
    </button>
  );
}
