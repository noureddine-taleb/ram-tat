import { CardWithDetails } from "views/components/CardWithDetails";
import { SubTask } from "views/components/SubTask";
import { TatProgressBar } from "views/components/TatProgressBar";
import React, { useEffect, useState } from "react";
import { TasksFilter } from "./TasksFilter";
import { useParams } from "react-router";
import { myTatTasks, tasksGroupByGroup } from "shared/services/TatService";
import { Task } from "shared/models/Task";
import { TaskFilter } from "shared/models/TaskFilter";
import { taskTime } from "shared/helpers/misc";

export function MyTasks() {
  const { tatId } = useParams();
  const [tasks, setTasks] = useState<Task[][]>([]);
  const [error, setError] = useState<string>();
  const [filter, setFilter] = useState<TaskFilter>(
    new TaskFilter(tatId as string)
  );

  useEffect(() => {
    myTatTasks(filter.getQueryParams())
      .then(tasksGroupByGroup)
      .then(setTasks)
      .catch((e) => {
        if (e.response.status === 403) {
          setError("vous n'avez pas l'accès à ce TAT");
        }
      });
  }, [tatId, filter]);

  return (
    <div className="flex flex-col">
      {error && (
        <div className="text-red-500 font-bold w-full flex justify-center">
          {error}
        </div>
      )}
      {!error && (
        <TasksFilter
          extraFilters
          filter={filter}
          setFilter={setFilter}
        ></TasksFilter>
      )}
      {!error &&
        tasks.map((group, i) => (
          <CardWithDetails
            key={group[0].idTask}
            main={
              <>
                <div className="flex justify-between gap-1">
                  <span className="font-bold">{group[0].groupTask}</span>
                </div>
                <TatProgressBar
                  className="w-3/12"
                  progress={40}
                  startTime={"--:--"}
                  endTime={"--:--"}
                ></TatProgressBar>
              </>
            }
            details={
              <>
                {group.map((task, i) => (
                  <>
                    <SubTask
                      key={task.id}
                      title={task.name}
                      progress={40}
                      startTime={taskTime(task.startDateCalculated)}
                      endTime={taskTime(task.endDateCalculated)}
                      className="flex lg:flex-row flex-col gap-y-4 w-full items-center bg-[#f4f4f4]"
                    />
                    <div className="w-full h-[2px] bg-[#d7d7d7] my-3"></div>
                  </>
                ))}
              </>
            }
          ></CardWithDetails>
        ))}
    </div>
  );
}
