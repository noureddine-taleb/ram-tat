import React, { useState } from "react";
import { TaskFilter } from "shared/models/TaskFilter";
import { Button } from "./Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

interface FilterProps {
  extraFilters: boolean;
  regroup?: boolean;
  filter: TaskFilter;
  setFilter: (f: TaskFilter) => void;
}

export function TasksFilter({
  extraFilters,
  regroup,
  filter,
  setFilter,
}: FilterProps) {
  const [show, setShow] = useState(false);

  return (
    <div className="flex flex-col px-4">
      {regroup && (
        <div className="flex w-full bg-white gap-5 mb-5">
          <Button
            fullWidth
            onClick={() => setFilter(filter.setGroupBy("familly"))}
            variant={
              filter.getGroupBy() === "familly" ? "contained" : "outlined"
            }
          >
            Regrouper par famille de tache
          </Button>
          <Button
            fullWidth
            onClick={() => setFilter(filter.setGroupBy("role"))}
            variant={filter.getGroupBy() === "role" ? "contained" : "outlined"}
          >
            Regrouper par role d'intervenant
          </Button>
        </div>
      )}
      <div className="flex flex-col gap-3 bg-gray-100 rounded-md overflow-hidden">
        <div className="bg-gray-200 h-10 flex justify-center items-center rounded-md">
          <div className="flex gap-1 h-8 w-[99%]">
            <Button
              fullWidth
              onClick={() => setFilter(filter.setPhase("ARRIVEE"))}
              variant={filter.getPhase() === "ARRIVEE" ? "contained" : "text"}
            >
              Vol Arrivee
            </Button>
            <Button
              fullWidth
              onClick={() => setFilter(filter.setPhase("DEPART"))}
              variant={filter.getPhase() === "DEPART" ? "contained" : "text"}
            >
              Vol Depart
            </Button>

            {extraFilters && (
              <Button
                className="w-16"
                onClick={() => setShow(!show)}
                variant={show ? "contained" : "outlined"}
              >
                <FontAwesomeIcon icon="filter" className="font-[20px]" />
              </Button>
            )}
          </div>
        </div>
        {extraFilters && (
          <div
            className={`flex justify-center items-center pb-5 ${
              !show && "hidden"
            }`}
          >
            <div className="flex w-[99%] gap-1">
              <div className="flex flex-col w-2/4 h-full">
                <div className="font-bold pb-2">STATUT</div>
                <div className="flex gap-2">
                  <Button
                    fullWidth
                    onClick={() => setFilter(filter.setStatus("FINISHED"))}
                    variant={
                      filter.getStatus() === "FINISHED"
                        ? "contained"
                        : "outlined"
                    }
                  >
                    Termine
                  </Button>
                  <Button
                    fullWidth
                    onClick={() => setFilter(filter.setStatus("STARTED"))}
                    variant={
                      filter.getStatus() === "STARTED"
                        ? "contained"
                        : "outlined"
                    }
                  >
                    En Cours
                  </Button>
                  <Button
                    fullWidth
                    onClick={() => setFilter(filter.setStatus("NON_STARTED"))}
                    variant={
                      filter.getStatus() === "NON_STARTED"
                        ? "contained"
                        : "outlined"
                    }
                  >
                    Non Demarre
                  </Button>
                </div>
              </div>
              <div className="flex flex-col w-2/4">
                <div className="font-bold pb-2">FAMILLE DE TACHES</div>
                <div className="flex gap-2">
                  <select
                    className="w-full rounded-md"
                    value={filter.getGroup()}
                    onChange={(e) =>
                      setFilter(filter.setGroup(e.target.value as any))
                    }
                  >
                    <option value="">ALL</option>
                    <option value="TRAITEMENT_AVION">TRAITEMENT_AVION</option>
                    <option value="PASSAGE">PASSAGE</option>
                    <option value="PNC">PNC</option>
                    <option value="CATERING">CATERING</option>
                    <option value="MAINTENANCE">MAINTENANCE</option>
                    <option value="PNT">PNT</option>
                    <option value="TIERS_1">TIERS_1</option>
                    <option value="TIERS_2">TIERS_2</option>
                  </select>
                  <Button
                    fullWidth
                    onClick={() => setFilter(filter.setDelayed(true))}
                    variant={filter.getDelayed() ? "contained" : "outlined"}
                  >
                    Taches En Retard
                  </Button>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
