import React, { useState } from "react";
import { TaskFilter } from "shared/models/TaskFilter";
import { TasksFilter } from "./components/TasksFilter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function Resources() {
  const [filter, setFilter] = useState<TaskFilter>(new TaskFilter("00"));

  return (
    <div className="flex flex-col">
      <TasksFilter
        extraFilters={false}
        regroup={false}
        filter={filter}
        setFilter={setFilter}
      ></TasksFilter>
      <div className="flex flex-col rounded-md shadow-md p-2 mt-4 bg-gray-100">
        <div className="flex bg-[#c7292e] rounded-tr-md rounded-br-md  p-2 w-[fit-content] text-white font-bold justify-center my-2 -left-2 relative">
          Resources Members TAT
        </div>
        <table>
          <thead className="bg-white rounded-lg overflow-hidden">
            <tr>
              <th className="py-4">Nom et Prenom</th>
              <th className="py-4">Role</th>
              <th className="py-4">Actif</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="py-4">
                <div className="flex h-full justify-center relative">
                  <div className="bg-black rounded-lg p-2 absolute left-0 -top-2 w-9 flex justify-center">
                    <FontAwesomeIcon
                      icon="user"
                      className="text-white"
                    ></FontAwesomeIcon>
                  </div>
                  <div>lorem</div>
                </div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center items-center gap-8">
                  <div className="bg-green-400 py-2 px-4 rounded-md text-white">
                    <FontAwesomeIcon icon="check"></FontAwesomeIcon>
                  </div>
                  <FontAwesomeIcon
                    icon="envelope"
                    className="text-blue-400"
                  ></FontAwesomeIcon>
                </div>
              </td>
            </tr>
            <tr>
              <td className="py-4">
                <div className="flex h-full justify-center relative">
                  <div className="bg-black rounded-lg p-2 absolute left-0 -top-2 w-9 flex justify-center">
                    <FontAwesomeIcon
                      icon="user"
                      className="text-white"
                    ></FontAwesomeIcon>
                  </div>
                  <div>lorem</div>
                </div>
              </td>
              <td className="py-4">
                <div className="flex justify-center font-medium">lorem</div>
              </td>
              <td className="py-4">
                <div className="flex justify-center items-center gap-8">
                  <div className="bg-green-400 py-2 px-4 rounded-md text-white">
                    <FontAwesomeIcon icon="check"></FontAwesomeIcon>
                  </div>
                  <FontAwesomeIcon
                    icon="envelope"
                    className="text-blue-400"
                  ></FontAwesomeIcon>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}
