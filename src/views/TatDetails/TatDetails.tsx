import React from "react";
import { Link, Outlet, useLocation } from "react-router-dom";
import { Tabs } from "views/components/Tabs";
import { TatDetailsTabs } from "data/TatDetailsTabs";

export function TatDetails() {
  const location = useLocation();
  const items = location.pathname.split("/");
  const value = items[4] || "";

  return (
    <div className="w-full">
      <Tabs value={value} items={TatDetailsTabs} />

      <div className="mt-24 font-lato">
        <Outlet></Outlet>
      </div>
    </div>
  );
}
