import { useEffect, useState } from "react";
import { getTasksList } from "shared/services/RefService";
import { Task } from "shared/models/Task";
import { IconEye } from "components/Icons/IconEye";
import { IconPencil } from "components/Icons/IconPencil";
import { ButtonIcon } from "./components/ButtonIcon";
import { IconPreference } from "components/Icons/IconPreference";
import { IconSearch } from "components/Icons/IconSearch";
import { IconPlus } from "components/Icons/IconPlus";
import { Link } from "react-router-dom";
import { TaskTable } from "data/Task";

export function TaskList() {
  const [taskList, setTaskList] = useState<Task[]>([]);
  const [filtTaskList, setfiltTaskList] = useState<Task[]>([]);
  const [queryField, setQueryField] = useState("name");
  const [queryStr, setQueryStr] = useState("");

  const getTasks = () => {
    getTasksList().then(setTaskList);
  };

  useEffect(() => {
    getTasks();
  }, []);

  useEffect(() => {
    setfiltTaskList(
      taskList.filter(
        (task) =>
          (task as any)[queryField]
            .toLowerCase()
            .indexOf(queryStr.toLowerCase()) !== -1
      )
    );
  }, [taskList, queryStr, queryField]);

  return (
    <div className="min-w-[800px] overflow-auto">
      <div className="rounded-lg overflow-hidden">
        <div className="w-full h-24 bg-white flex items-center justify-between px-3">
          <Link to="/dashboard/new-task">
            <button className="bg-[#2AD4B9] flex rounded-lg w-72 h-11 items-center justify-center px-3 text-white font-bold text-base">
              <IconPlus className="w-7 h-7 mr-9" />
              Ajouter une Tache
            </button>
          </Link>
          <div className="flex gap-1">
            <input
              className="hover:outline-none border border-[#929292] border-opacity-50 appearance-none rounded pl-5 placeholder-[#929292] h-11"
              placeholder="Recherche"
              type="text"
              value={queryStr}
              onChange={(e) => setQueryStr(e.target.value)}
            />
            <select
              value={queryField}
              onChange={(e) => setQueryField(e.target.value)}
            >
              <option value="name">name</option>
              <option value="idTask">ID</option>
              <option value="groupTask">group</option>
              <option value="phase">phase</option>
              <option value="type">type</option>
            </select>
          </div>
        </div>
        <div className="p-6 bg-gradient-to-br from-[#F4F4F4] to-white">
          <table className="table-auto w-full">
            <thead className="border-b-[1px] border-[#7D001C]">
              <tr className="font-bold text-sm leading-5 text-[#7D001C]">
                {TaskTable.map((h, i) => (
                  <th key={i} className="h-14">
                    {h}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody className="font-normal text-xs not-italic text-center">
              {filtTaskList.map((task) => (
                <tr key={task.id}>
                  <td>{task.idTask}</td>
                  <td>{task.name}</td>
                  <td>{task.groupTask}</td>
                  <td>{task.phase}</td>
                  <td>{task.type}</td>
                  <td className="flex justify-center my-3 gap-1">
                    <ButtonIcon className="bg-[#2AD4B9]">
                      <IconEye />
                    </ButtonIcon>
                    <ButtonIcon className="bg-[#FDBC64]">
                      <IconPencil className="w-[13px] h-[13px]" />
                    </ButtonIcon>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <div className="flex">
        <div className="ml-auto">{/*<Pagination />*/}</div>
      </div>
    </div>
  );
}
