import { Tat, TAT_TYPE } from "shared/models/Tat";
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { TatId } from "./TatId";
import { TatOperations } from "./TatOperations";
import { TatDetails } from "./TatDetails";
import { CardWithDetails } from "views/components/CardWithDetails";
import { TatDetailsElem } from "./TatDetailsElem";
import { Task } from "shared/models/Task";
import { getTatTasks, tasksGroupByGroup } from "shared/services/TatService";

function getTatStatusImg(tat: Tat) {
  switch (tat.TATType()) {
    case TAT_TYPE.TAT_TYPE_ARR:
      return "/static/TATArr.png";
    case TAT_TYPE.TAT_TYPE_DEP:
      return "/static/TATDep.png";
    case TAT_TYPE.TAT_TYPE_ARRDEP:
      return "/static/TATArrDep.png";
  }
}

export function TatCard({ tat }: { tat: Tat }) {
  const [show, setShow] = useState(false);
  const [task, setTask] = useState<{ [key: string]: Task[] }>({});

  useEffect(() => {
    getTatTasks(`tatId=${tat.getId().toString()}&groupByGroupTask=true`).then(
      setTask
    );
  }, [tat.getId()]);

  return (
    <CardWithDetails
      main={
        <>
          <Link to={`${tat.getId()}`} className="text-black">
            <TatId
              className="flex"
              tatImg={getTatStatusImg(tat)}
              id={tat.getACId()}
              isRam={tat.isRAM()}
              legState={tat.getLegState()}
            />
          </Link>
          <div className="bg-[#d7d7d7] h-full w-1 rounded-md"></div>
          <div className="flex w-full flex-col xl:flex-row gap-8">
            <Link to={`${tat.getId()}`}>
              <TatDetails
                tat={tat}
                className="flex flex-col items-center xl:ml-4"
              />
            </Link>

            <TatOperations
              tat={tat}
              className="flex lg:flex-row flex-col xl:ml-auto gap-5 lg:mr-5 xl:w-2/5"
            />
          </div>
        </>
      }
      details={
        <>
          {Object.keys(task).map((group, i) => (
            <>
              <TatDetailsElem
                key={group + i + i}
                group={task[group]}
                className="flex lg:flex-row flex-col gap-y-4 w-full items-center bg-[#f4f4f4]"
              />
              <div className="w-full h-[2px] bg-[#d7d7d7] my-3"></div>
            </>
          ))}
        </>
      }
    ></CardWithDetails>
  );
}
