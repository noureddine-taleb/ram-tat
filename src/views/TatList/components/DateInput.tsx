import { TAT_DATE_FORMAT } from "config/config";
import moment from "moment";
import { useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function DateInput({
  name,
  id,
  label,
  placeholder,
  className,
  onChange,
  value,
}: {
  name: string;
  id: string;
  label: string;
  placeholder: string;
  className?: string;
  value?: Date | null;
  onChange: (d: Date) => void;
}) {
  const dateRef = useRef<HTMLInputElement>(null);

  return (
    <div
      className={`flex flex-col relative cursor-pointer ${className}`}
      onClick={() => {
        dateRef.current?.focus();
        dateRef.current?.click();
        const e = document.createEvent("KeyboardEvent");
        (e as any).initKeyboardEvent(
          "keydown",
          true,
          true,
          document.defaultView,
          "F4",
          0
        );
        dateRef.current?.dispatchEvent(e);
      }}
    >
      <div className="flex justify-between border-b border-b-blue-50 pb-4 items-center">
        <div className="text-gray-300 text-[20px]">
          {value ? moment(value).format(TAT_DATE_FORMAT) : label}
        </div>
        <div>
          <FontAwesomeIcon
            icon="calendar-alt"
            className="text-white text-[40px]"
          />
        </div>
      </div>
      <input
        className="absolute w-full h-full opacity-0 cursor-pointer"
        type="date"
        id={id}
        name={name}
        placeholder={placeholder}
        ref={dateRef}
        onChange={() => {
          onChange(dateRef.current?.valueAsDate as Date);
        }}
        value={moment(value).format("YYYY/MM/DD")}
      />
    </div>
  );
}
