export function Buttons({children, label}: {children: any; label: string}) {
	return (
		<div className="flex flex-col w-full pb-5">
			<div className="uppercase text-white font-bold mb-5">{label}</div>
			<div className="flex gap-2 h-9">{children}</div>
		</div>
	);
}
