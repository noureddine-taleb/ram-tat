import {TatFlight} from './TatFlight';
import {Tat} from 'shared/models/Tat';
import React from 'react';

export function TatDetails({tat, className}: {tat: Tat; className: string}) {
	return (
		<div className={className}>
			<div className="flex lg:flex-row flex-col lg:self-start items-center gap-y-4 justify-around xl:justify-between w-full">
				{tat.isArr() && (
					<TatFlight
						type="ARRIVAL"
						FN={tat.getArrFN()}
						DepAP={tat.getArrAP_dep()}
						ArrAP={tat.getArrAP_arr()}
						date={tat.getArrDate()}
						time={tat.getArrTime()}
						image="/static/PTARR.png"
					/>
				)}
				{tat.isArr() && tat.isDep() && (
					<div className="h-full w-12 sm:flex hidden mx-8 justify-center items-center">
						<img className="object-cover" src="/static/arrow.png" alt="" />
					</div>
				)}
				{tat.isDep() && (
					<TatFlight
						type="DEPARTURE"
						FN={tat.getDepFN()}
						DepAP={tat.getDepAP_dep()}
						ArrAP={tat.getDepAP_arr()}
						date={tat.getDepDate()}
						time={tat.getDepTime()}
						image="/static/PTDEP.png"
					/>
				)}
			</div>
			<div className="bg-[#d7d7d7] h-[2px] min-w-full"></div>
			<div className="flex items-center mt-3 justify-between w-72">
				<div className="flex gap-1">
					<img
						className="max-w-[20px]"
						src="/static/icons8-time-64px-copie-2.png"
						alt=""
					/>
					<div className="">60 min</div>
				</div>
				<div className="flex gap-1">
					<img
						className="max-w-[20px]"
						src="/static/icons8-parking-and-penthouse-64px-copie-2.png"
						alt=""
					/>
					<div className="">Parcking</div>
				</div>
				<div className="flex gap-1">
					<img
						className="max-w-[20px]"
						src="/static/icons8-tollbooth-64px-copie-2.png"
						alt=""
					/>
					<div className="">Gate</div>
				</div>
			</div>
		</div>
	);
}
