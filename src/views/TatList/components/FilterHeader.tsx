import { SearchBar } from "./SearchBar";
import { DateInput } from "./DateInput";
import { Buttons } from "./Buttons";
import { FilterButton } from "./FilterButton";
import { useState } from "react";
import { Filter, interval } from "shared/models/Filter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function FilterHeader({
  filters,
  setFilters,
}: {
  filters: Filter;
  setFilters: React.Dispatch<React.SetStateAction<Filter>>;
}) {
  const [showFilter, setShowFilter] = useState<boolean>(false);

  return (
    <div className="w-full pb-10 bg-gradient-to-r from-[#f0405c] to-[#c20831] flex flex-col justify-center items-center gap-5 px-8 relative pt-2">
      <div className="flex h-10 w-full gap-4 lg:flex-row flex-col lg:mb-0 mb-20">
        <div className="flex w-full h-full gap-2 items-center justify-center">
          <button
            onClick={() => setFilters(filters.setMyTat(!filters.getMyTat()))}
            className={`${
              filters.getMyTat()
                ? "bg-white text-green-500"
                : "bg-transparent text-white"
            } w-[25%] h-full border border-white text-sm rounded-md px-3 font-bold`}
          >
            MES TAT
          </button>
          <SearchBar
            className="w-[80%]"
            onChange={(e) => setFilters(filters.setQuery(e.target.value))}
            value={filters.getQuery() || ""}
          />
        </div>
        <div className="flex w-full h-full gap-2 items-center justify-center">
          <select
            className="w-[100%] h-full bg-white rounded-md px-3"
            onChange={(e) =>
              setFilters(filters.setInterval(e.target.value as interval))
            }
            value={filters.getInterval() as string}
          >
            <option value="all">all flights</option>
            <option value="next-12">prochaines 12 heures</option>
            <option value="next-3">prochaines 3 heures</option>
            <option value="next-6">prochaines 6 heures</option>
            <option value="prev-12">précédentes 12 heures</option>
          </select>
          <button
            className="bg-[#d40b3f] text-white w-10 h-10 rounded-md flex justify-center items-center"
            onClick={() => setShowFilter(!showFilter)}
          >
            <FontAwesomeIcon icon="filter" className="font-[20px]" />
            <FontAwesomeIcon icon="sort-down" className="w-[20px] h-[20px]" />
          </button>
        </div>
      </div>
      {showFilter && (
        <div className="w-full gap-y-20">
          <div className="flex xl:flex-row flex-col justify-center items-center w-full gap-4 mb-5">
            <DateInput
              className="w-full"
              name="date-start"
              id="date-start"
              label="Date de debut"
              placeholder="Date de debut"
              onChange={(d: Date) => setFilters(filters.setStartDate(d))}
              value={filters.getStartDate() && filters.getStartDate()}
            />
            <div className="xl:block hidden">
              <FontAwesomeIcon
                icon="arrow-right"
                className="text-white font-[30px]"
              />
              <FontAwesomeIcon
                icon="arrow-left"
                className="text-white font-[30px]"
              />
            </div>
            <DateInput
              className="w-full"
              name="date-end"
              id="date-end"
              label="Date de fin"
              placeholder="Date de fin"
              onChange={(d: Date) => setFilters(filters.setEndDate(d))}
              value={filters.getEndDate() && filters.getEndDate()}
            />
          </div>
          <div className="flex xl:flex-row flex-col justify-center items-center w-full gap-10 mb-5">
            <Buttons label="type de vol">
              <FilterButton
                label="RAM"
                selected={filters.getCompany() === "ram"}
                onClick={() => setFilters(filters.setCompany("ram"))}
              />
              <FilterButton
                label="NO RAM"
                selected={filters.getCompany() === "noram"}
                onClick={() => setFilters(filters.setCompany("noram"))}
              />
              <FilterButton
                label="ALL"
                selected={!filters.getCompany()}
                onClick={() => setFilters(filters.setCompany(null))}
              />
            </Buttons>

            <Buttons label="alerts">
              <FilterButton
                selected={filters.getAlert() === "whith-alerts"}
                onClick={() => setFilters(filters.setAlert("whith-alerts"))}
                label="Avec Alertes"
              />
              <FilterButton
                selected={filters.getAlert() === "my-alerts"}
                onClick={() => setFilters(filters.setAlert("my-alerts"))}
                label="Mes Alertes"
              />
              <FilterButton
                selected={filters.getAlert() === "w/o-alerts"}
                onClick={() => setFilters(filters.setAlert("w/o-alerts"))}
                label="Sans Alertes"
              />
            </Buttons>
          </div>
          <div className="flex xl:flex-row flex-col justify-center items-center w-full gap-10 mb-5">
            <Buttons label="type tat">
              <FilterButton
                selected={filters.getTat().includes("Arr")}
                onClick={() => setFilters(filters.toggleTat("Arr"))}
                label="Arrivées seules"
              />
              <FilterButton
                selected={filters.getTat().includes("Dep")}
                onClick={() => setFilters(filters.toggleTat("Dep"))}
                label="Départ Seules"
              />
              <FilterButton
                selected={filters.getTat().includes("ArrDep")}
                onClick={() => setFilters(filters.toggleTat("ArrDep"))}
                label="Touchées"
              />
            </Buttons>
          </div>
          <div className="flex justify-center items-center w-full gap-10 h-[40px] shadow-md mb-5">
            <button
              onClick={() => setFilters(filters.clear())}
              className="bg-[#d40808] text-white rounded-md w-full h-full font-bold text-[20px]"
            >
              Clear
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
