export function TatId({
	isRam,
	tatImg,
	id,
	legState,
	className,
}: {
	isRam: boolean;
	tatImg: string;
	id: string;
	legState: string;
	className: string;
}) {
	return (
		<div className={className}>
			<div className="mr-5">
				{isRam && (
					<div className="flex p-2 justify-end bg-white rounded-tr-md rounded-br-md">
						<img
							className="w-20 h-14"
							src="/static/1200px-royal-air-maroc-logo-svg-copie-2.png"
							alt=""
						/>
					</div>
				)}
				<div className="w-28 m-5">{<img src={tatImg} alt="" />}</div>
			</div>
			<div className="flex flex-col justify-between font-bold min-w-[160px]">
				<div className="flex ml-1">
					<div className="w-16">
						<img className="w-6 h-6" src="/static/charging-circle.png" alt="" />
					</div>
					<div className="bg-ram-green rounded-md text-white px-1 font-bold">
						ON TIME
					</div>
				</div>
				<div className="h-5 flex">
					<div className="w-16">
						<img
							className="h-5"
							src="/static/icons8-airplane-64px-copie-2.png"
							alt=""
						/>
					</div>
					<div>{id}</div>
				</div>
				<div className="flex">
					<div className="w-16">
						<img
							className="h-6"
							src="/static/icons8-skyscrapers-64px-copie-2.png"
							alt=""
						/>
					</div>
					<div>{isRam ? 'RAM' : 'NOT-RAM'}</div>
				</div>
				<div className="flex">
					<div className="w-16">
						<img
							className="w-7"
							src="/static/icons8-runway-64px-copie-2.png"
							alt=""
						/>
					</div>
					<div>{legState}</div>
				</div>
			</div>
		</div>
	);
}
