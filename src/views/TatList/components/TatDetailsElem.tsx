import { Task } from "shared/models/Task";

export function TatDetailsElem({
  className,
  group,
}: {
  className: string;
  group: Task[];
}) {
  return (
    <div className={className}>
      <div className="flex gap-3 w-full items-center">
        <div>
          <img src="/static/icons8_runway_48px.png" alt="" />
        </div>
        <div className="flex flex-col">
          <div className="font-bold">{group[0].groupTask}</div>
          <div className="text-[12px]">
            <ul>
              {group.map((task) => (
                <li key={task.idTask}>{task.name}</li>
              ))}
            </ul>
          </div>
        </div>
      </div>

      <div className="flex gap-3 w-full flex-col">
        <div className="w-full bg-[#4ed19e] h-3 rounded-xl"></div>
        <div className="flex justify-between font-semibold">
          <div>18:40</div>
          <div className="text-[#4ed19e]">+2min</div>
          <div>19:00</div>
        </div>
      </div>

      <div className="flex gap-3 w-full font-semibold justify-between lg:justify-around items-center">
        <div className="text-[#4ed19e]">18:40</div>
        <button className="bg-[#4ed19e] w-24 h-9 rounded-lg font-semibold text-white">
          terminer
        </button>
      </div>
    </div>
  );
}
