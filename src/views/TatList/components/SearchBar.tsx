import {IconSearch} from 'components/Icons/IconSearch';
import {useRef} from 'react';

export function SearchBar({
	className,
	value,
	onChange,
}: {
	className: string;
	value: string;
	onChange: React.ChangeEventHandler<HTMLInputElement>;
}) {
	const searchInput = useRef<HTMLInputElement>(null);
	//if (clear && searchInput.current) searchInput.current.value = '';

	return (
		<div className={`${className} relative`}>
			<div className="absolute top-2 left-3">
				<IconSearch className="text-black w-6 h-5" />
			</div>
			<input
				value={value}
				onChange={onChange}
				className="h-[40px] w-full bg-white rounded-md max-w-full appearance-none border
			focus:outline-none pl-10 placeholder-current"
				type="text"
				placeholder="Rechercher"
				name=""
				id=""
				ref={searchInput}
			/>
		</div>
	);
}
