import {useState} from 'react';

export function FilterButton({
	className,
	label,
	onClick,
	selected,
}: {
	className?: string;
	label: string;
	onClick: () => void;
	selected: boolean;
}) {
	//const [selected, setSelected] = useState<boolean>(false);

	return (
		<button
			onClick={onClick}
			className={`${
				selected
					? 'bg-white text-[#ca1d41]'
					: 'bg-transparent text-gray-200 border'
			}  rounded-md w-full font-bold text-[20px] h-[fit-content]`}>
			{label}
		</button>
	);
}
