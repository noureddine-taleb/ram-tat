import { useState } from "react";

function FlightData({ className }: { className: string }) {
  return (
    <div className={className}>
      <div className="w-[70%] bg-[#d24343] flex flex-col text-white justify-around text-[11px] items-center">
        <div className="flex justify-center items-center gap-1">
          {/*<FlightIcon />*/}
          Passengers
        </div>
        <div className="w-11/12 h-[1px] self-center bg-white"></div>
        <div className="flex justify-center items-center gap-1">
          {/*<PassengerIcon />*/}
          load sheet
        </div>
      </div>
      <div className="w-[30%] flex flex-col text-[#d24343] justify-around text-[11px] items-center bg-white">
        122
        <div className="w-11/12 h-[1px] self-center bg-[#d24343]"></div>
        1452
      </div>
    </div>
  );
}

export function TatFlight({
  type,
  FN,
  DepAP,
  ArrAP,
  date,
  time,
  image,
}: {
  type: "ARRIVAL" | "DEPARTURE";
  FN: string;
  DepAP: string;
  ArrAP: string;
  date: string;
  time: string;
  image: string;
}) {
  const [show, setShow] = useState(false);
  return (
    <div className="flex flex-col w-[fit-content]">
      <div className="flex items-center gap-5">
        <div className="w-6 h-6 self-start pt-3">
          <img src={image} alt="" />
        </div>
        <div className="font-bold flex flex-col">
          <div className="flex justify-between">
            {type}
            {type === "DEPARTURE" && (
              <div className="relative flex items-center">
                <div
                  onClick={() => setShow(!show)}
                  className="w-5 h-5 border-2 border-red-600 rounded-full flex justify-center items-center text-red-600 cursor-pointer pb-1"
                >
                  i
                </div>
                {show && (
                  <FlightData className="md:left-8 md:top-[unset] -top-20 -left-32 absolute min-w-[128px] min-h-[64px] flex rounded-md overflow-hidden shadow-lg" />
                )}
              </div>
            )}
          </div>
          <div className="text-[#a11635]">{FN}</div>
          <div className="flex gap-1 items-center">
            <div>{DepAP}</div>
            <div className="bg-[#a81038] rounded-full w-1 h-1"></div>
            <div className="w-8 flex">
              <img src="/static/flight-path.png" alt="" />
            </div>
            <div className="bg-[#a81038] rounded-full w-1 h-1"></div>
            <div>{ArrAP}</div>
          </div>
          <div className="">
            <div className="">
              <span className="font-bold">{time}</span>
              <span className="font-thin">&nbsp; GMT</span>
            </div>
            <div className="font-thin mt-1">{date}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
