import { Tat } from "shared/models/Tat";
import { acquireTat } from "shared/services/TatService";

export function TatOperations({
  className,
  tat,
}: {
  className: string;
  tat: Tat;
}) {
  return (
    <div className={className}>
      <div className="flex flex-col w-full">
        <button
          onClick={() => acquireTat(tat.getId().toString()).then(console.log)}
          className={`${
            !tat.autoAffect && "opacity-0"
          } flex justify-center items-center h-10 w-36 rounded-md border border-red-600 mx-auto lg:ml-auto lg:mr-0`}
        >
          <div className=" font-bold text-ram-shiraz">S'AFFECTER</div>
        </button>

        <button
          className={`${
            !tat.liberate && "opacity-0"
          } flex justify-center items-center h-10 w-36 rounded-md border border-red-600 mx-auto lg:ml-auto lg:mr-0`}
        >
          <div className=" font-bold text-ram-shiraz">LIBERER</div>
        </button>

        <div className="flex flex-col justify-end w-full h-[97px]">
          <div className="flex justify-between">
            <h1 className="text-[#4ed19e] font-semibold">100%</h1>
            <div className="font-semibold">TAT ON TIME</div>
          </div>
          <div className="bg-[#4ed19e] rounded-lg h-3 w-full"></div>
          <div className="mt-2 text-ram-green">00 : 12 : 30</div>
        </div>
      </div>
      <div className="flex items-end justify-center lg:mr-5"></div>
    </div>
  );
}
