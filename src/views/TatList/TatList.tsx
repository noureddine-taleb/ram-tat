import React, { useEffect, useState } from "react";
import { FilterHeader } from "./components/FilterHeader";
import { Filter } from "shared/models/Filter";
import { TatCard } from "./components/TatCard";
import { Tat } from "shared/models/Tat";
import { Paging } from "shared/models/Response";
import { setImmediateInterval } from "shared/helpers/misc";
import { getTats } from "shared/services/TatService";
import { API_PROBE_DELAY } from "config/config";
import { Pagination } from "views/components/Pagination";

export function TatList() {
  const [filters, setFilters] = useState<Filter>(new Filter());
  const [tats, setTats] = useState<Tat[]>([]);
  const [paging, setPaging] = useState<Paging>({} as Paging);

  useEffect(() => {
    const intervalId = setImmediateInterval(() => {
      getTats(filters.getQueryParams(), setPaging, setTats).catch(
        console.error
      );
    }, API_PROBE_DELAY);

    return () => clearInterval(intervalId);
  }, [filters]);

  return (
    <div className="flex flex-col">
      <FilterHeader filters={filters} setFilters={setFilters} />
      <div className="self-center flex flex-col justify-center items-center pb-3 pt-10 w-full px-7">
        {tats.map((tat) => (
          <TatCard key={tat.getId()} tat={tat} />
        ))}
        <Pagination paging={paging} filters={filters} setFilters={setFilters} />
      </div>
    </div>
  );
}
