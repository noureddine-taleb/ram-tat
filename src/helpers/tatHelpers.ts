export function getProgressTextColor(progress: number): string {
	if (progress === 0) return 'text-gray-400';
	else if (progress === 100) return 'text-ram-green';
	else return 'text-blue-500';
}

export function getProgressBgColor(progress: number): string {
	if (progress === 0) return 'bg-gray-400';
	else if (progress === 100) return 'bg-ram-green';
	else return 'bg-blue-500';
}

export function getProgressDesc(progress: number): string {
	if (progress === 0) return 'Non Demarre';
	else if (progress === 100) return 'Terminer';
	else return 'En Cours';
}
