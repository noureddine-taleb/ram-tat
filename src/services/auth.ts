export const login = () => {
  let port = window.location.port ? ":" + window.location.port : "";
  window.location.href = "//" + window.location.hostname + port + "/private";
};

export function logout() {
  const el: HTMLFormElement | null = document.getElementById(
    "logout-form"
  ) as HTMLFormElement;
  el?.submit();
}

export async function getUser() {
  const response = await fetch("/api/user");
  const body = await response.text();
  return body;
}
