import React from "react";
import { Outlet } from "react-router";
import { Header } from "components/Header/Header";
import { NavBar } from "components/NavBar/NavBar";
import { NavBarElems } from "data/NavBarElems";

export function DashboardLayout() {
  return (
    <div className="flex flex-row h-[100vh] w-[100vw] bg-contain bg-ram-red bg-opacity-5">
      <NavBar list={NavBarElems} />
      <div className="flex flex-col w-full">
        <Header list={NavBarElems} />
        <main className="relative w-full overflow-y-auto">
          <Outlet />
        </main>
      </div>
    </div>
  );
}
