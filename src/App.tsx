import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Routes } from "routes";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { useCookies } from "react-cookie";
import axios from "axios";
import "./App.css";
import { getUser, login, logout } from "services/auth";

axios.interceptors.request.use((req) => {
  async function runAsync() {
    if ((await getUser()) === "") {
      login();
    }
  }
  runAsync();

  return req;
});

axios.interceptors.response.use(undefined, async (error) => {
  if (error.response?.status === 401) {
    logout();
  }
  return Promise.reject(error);
});

library.add(fas);
export default function App() {
  const [cookies] = useCookies(["XSRF-TOKEN"]);

  return (
    <BrowserRouter>
      <form id="logout-form" className="hidden" action="/logout" method="POST">
        <input type="hidden" name="_csrf" value={cookies["XSRF-TOKEN"]} />
        <button type="submit">logout</button>
      </form>
      <Routes></Routes>
    </BrowserRouter>
  );
}
