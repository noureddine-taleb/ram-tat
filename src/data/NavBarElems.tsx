import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavElement } from "interfaces/NavElement";

export const NavBarElems: NavElement[] = [
  {
    icon: (
      <FontAwesomeIcon className="h-5 w-5" icon="list-ol"></FontAwesomeIcon>
    ),
    label: "Liste des Tat",
    link: "/dashboard/tat",
  },
  {
    icon: (
      <FontAwesomeIcon
        className="h-5 w-5"
        icon="clipboard-list"
      ></FontAwesomeIcon>
    ),
    label: "Liste des Taches",
    link: "/dashboard/task",
  },
];
