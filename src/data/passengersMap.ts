import {RowSeatsMap} from 'interfaces/PassengerSeatMap';

export const busPassengersMap: RowSeatsMap[] = [
	{
		row: 1,
		leftMap: [
			{
				id: 'A',
				status: true,
			},
			{
				id: 'C',
				status: false,
			},
		],
		rightMap: [
			{
				id: 'D',
				status: true,
			},
			{
				id: 'F',
				status: true,
			},
		],
	},
	{
		row: 1,
		leftMap: [
			{
				id: 'A',
				status: true,
			},
			{
				id: 'C',
				status: false,
			},
		],
		rightMap: [
			{
				id: 'D',
				status: true,
			},
			{
				id: 'F',
				status: true,
			},
		],
	},
];

export const econPassengersMap: RowSeatsMap[] = [
	{
		row: 1,
		leftMap: [
			{
				id: 'A',
				status: true,
			},
			{
				id: 'B',
				status: false,
			},
			{
				id: 'C',
				status: true,
			},
		],
		rightMap: [
			{
				id: 'D',
				status: true,
			},
			{
				id: 'E',
				status: false,
			},
			{
				id: 'F',
				status: true,
			},
		],
	},
	{
		row: 1,
		leftMap: [
			{
				id: 'A',
				status: true,
			},
			{
				id: 'B',
				status: false,
			},
			{
				id: 'C',
				status: true,
			},
		],
		rightMap: [
			{
				id: 'D',
				status: true,
			},
			{
				id: 'E',
				status: false,
			},
			{
				id: 'F',
				status: true,
			},
		],
	},
];
