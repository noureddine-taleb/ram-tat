import { TabElement } from "interfaces/TabElement";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const TatDetailsTabs: TabElement[] = [
  {
    label: "MES TACHES",
    to: "",
    icon: <FontAwesomeIcon icon="bars"></FontAwesomeIcon>,
  },

  {
    label: "VUE EQUIPE",
    to: "vue-equipe",
    icon: <FontAwesomeIcon icon="users"></FontAwesomeIcon>,
  },

  {
    label: "RESOURCES",
    to: "resources",
    icon: <FontAwesomeIcon icon="users-cog"></FontAwesomeIcon>,
  },

  {
    label: "VOL-INFO",
    to: "vol-infos",
    icon: <FontAwesomeIcon icon="plane"></FontAwesomeIcon>,
  },

  {
    label: "SLOT",
    to: "slot",
    icon: <FontAwesomeIcon icon="truck-loading"></FontAwesomeIcon>,
  },
];
