export interface NavElement {
  icon: JSX.Element;
  label: string;
  link: string;
}
