export interface TabElement {
  label: string;
  to: string;
  icon: JSX.Element;
}
