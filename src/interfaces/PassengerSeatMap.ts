export interface SeatStatus {
	id: string;
	status: boolean;
}

export interface RowSeatsMap {
	row: number;
	leftMap: SeatStatus[];
	rightMap: SeatStatus[];
}
