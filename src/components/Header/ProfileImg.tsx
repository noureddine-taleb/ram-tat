import { useEffect, useState } from "react";
import { getUser } from "services/auth";

export function ProfileImg() {
  const [user, setUser] = useState("");

  useEffect(() => {
    getUser().then(setUser);
  }, []);

  return (
    <div className="flex gap-2 items-center">
      <div className="flex-col hidden sm:flex">
        <span>{user}</span>
        <span>Role</span>
      </div>
      <img className="w-10 h-10 rounded-full bg-white" src="/logo.png" alt="" />
    </div>
  );
}
